#include <windows.h>
#include "MinHook.h"
#include "winusb.h"
#include <array>
#include <format>
#include <string>
#include <winscard.h>





static std::string defaultCardataString = "00000007020392012345678";
static std::string cardataString;
static bool cardInserted = false;

SCARDCONTEXT applicationContext;
LPSTR reader = NULL;
SCARDHANDLE connectionHandler;
DWORD activeProtocol;



static void establishContext() {
    LONG status = SCardEstablishContext(SCARD_SCOPE_SYSTEM, NULL, NULL, &applicationContext);
    if (status != SCARD_S_SUCCESS) {
        //OutputDebugStringA("Establish Context Error.\n");
        ExitThread(1);
    }
}
static void releaseContext() {
    LONG status = SCardReleaseContext(applicationContext);
    if (status != SCARD_S_SUCCESS) {
        //OutputDebugStringA("SCard Release Error.\n");
        ExitThread(1);
    }
}
static void listReaders() {
    DWORD readers = SCARD_AUTOALLOCATE;
    LONG status = SCardListReadersA(applicationContext, NULL, (LPSTR)&reader, &readers);

    if (status == SCARD_S_SUCCESS) {
        char* p = reader;
        while (*p) {
            p += strlen(p) + 1;
        }
    }
    else {
        //OutputDebugStringA("List Reader Error.\n");
        ExitThread(1);
    }
}
static void freeListReader() {
    SCARD_READERSTATE rgscState[MAXIMUM_SMARTCARD_READERS];


    LONG status = SCardFreeMemory(applicationContext, reader);
    if (status != SCARD_S_SUCCESS) {
        //OutputDebugStringA("Free Reader List Error.\n");
        ExitThread(1);
    }
}
static void waitForCard(){
    SCARD_READERSTATEA readerStates[MAXIMUM_SMARTCARD_READERS];
    readerStates[0].szReader = reader;
    readerStates[0].dwCurrentState = SCARD_STATE_EMPTY;
    DWORD readerStateCount = 1;

    while (0 != (readerStates[0].dwCurrentState & SCARD_STATE_EMPTY))
    {
        cardataString = defaultCardataString;
        cardInserted = false;

        SCardGetStatusChangeA(applicationContext, INFINITE, readerStates, readerStateCount);
        readerStates[0].dwCurrentState = readerStates[0].dwEventState;
        LONG status = SCardGetStatusChangeA(applicationContext, INFINITE, readerStates, readerStateCount);

        if (status != SCARD_S_SUCCESS) {
            //OutputDebugStringA("Card Wait Error.\n");
            ExitThread(1);
        }
        readerStates[0].dwCurrentState = readerStates[0].dwEventState;
       
    }
}
static void connectToCard() {
    activeProtocol = -1;

    LONG status = SCardConnectA(applicationContext, reader, SCARD_SHARE_SHARED, SCARD_PROTOCOL_T0 | SCARD_PROTOCOL_T1, &connectionHandler, &activeProtocol);
    if (status != SCARD_S_SUCCESS) {
        //OutputDebugStringA("Card Connection Error\n");
        ExitThread(1);
    }
}
static void disconnectFromCard() {
    LONG status = SCardDisconnect(connectionHandler, SCARD_LEAVE_CARD);
    if (status != SCARD_S_SUCCESS) {
        //OutputDebugStringA("Card Disconnect Error.");
        ExitThread(1);
    }
}

static void getNESiCASerialNo() {
    uint8_t command[] = { 0xFF, 0x00, 0x00, 0x00, 0x05, 0xD4, 0x40, 0x01, 0x30, 0x05 };
    std::string serialNo = "0000000";
    unsigned short commandLength = sizeof(command);
    const SCARD_IO_REQUEST* pioSendPci;
    SCARD_IO_REQUEST pioRecvPci;
    uint8_t response[300];
    unsigned long responseLength = sizeof(response);
    LONG status = SCardTransmit(connectionHandler, SCARD_PCI_T1, command, commandLength, NULL, response, &responseLength);

    if (status == SCARD_S_SUCCESS) {
        if (responseLength > 16) {
            for (int i = 3; i < responseLength - 2; i++) {
                serialNo.push_back(char(response[i]));
            }
        }
    }
    else {
        //OutputDebugStringA("Send Command Error.");
        ExitThread(1);
    }
    if (serialNo != "0000000") {
        cardataString = serialNo;
        cardInserted = true;
        //OutputDebugStringA("NESiCA Read!\n");
    }
}


static DWORD WINAPI CardFunctions(LPVOID)
{
    while (TRUE)
    {
        establishContext();
        listReaders();
        waitForCard();
        connectToCard();
        getNESiCASerialNo();
        disconnectFromCard();
        freeListReader();
        releaseContext();
        Sleep(1000);
    }
}











static DWORD WINAPI InsertCardThread(LPVOID)
{
    static bool keyDown;
    cardataString = defaultCardataString;
    while (true)
    {
        if (GetAsyncKeyState(0x50)& 0x01)
        {
            if (!keyDown)
            {
                cardInserted = !cardInserted;
                keyDown = true;
            }
        }
        else
        {
            keyDown = false;
        }
        Sleep(100);
    }
}

BOOL (* __fastcall gOrigWinUsb_ReadPipe)(
    WINUSB_INTERFACE_HANDLE InterfaceHandle,
    UCHAR PipeID,
    PUCHAR Buffer,
    ULONG BufferLength,
    PULONG LengthTransferred,
    LPOVERLAPPED Overlapped);

BOOL __fastcall WinUsb_ReadPipe_Wrap(
    WINUSB_INTERFACE_HANDLE InterfaceHandle,
    UCHAR PipeID,
    PUCHAR Buffer,
    ULONG BufferLength,
    PULONG LengthTransferred,
    LPOVERLAPPED Overlapped)
{
    //OutputDebugStringA("Success");
    auto ret = gOrigWinUsb_ReadPipe(InterfaceHandle, PipeID, Buffer, BufferLength, LengthTransferred, Overlapped);
    if (cardInserted)
    {
        
        memcpy(Buffer + BufferLength - 23, cardataString.c_str(), 23);
        Buffer[40] = 0x19;
    }
    return ret;
}

static DWORD WINAPI InitThread(LPVOID)
{
    Sleep(1000);
    const auto handle = reinterpret_cast<char*>(GetModuleHandleA("S6145-5Husb.dll"));
    if (handle == INVALID_HANDLE_VALUE)
    {
        OutputDebugStringA("GetModuleHandleA failed");
    }
    OutputDebugStringA(std::format("Handle address: 0x{:X}", (unsigned long long)handle).c_str());

    MH_Initialize();
    auto status = MH_CreateHook(handle+0x9010, WinUsb_ReadPipe_Wrap, reinterpret_cast<LPVOID*>(&gOrigWinUsb_ReadPipe));
    if (status != MH_OK)
    {
        OutputDebugStringA(std::format("MH_CreateHook failed, error: {:X}", (int)status).c_str());
    }
    MH_EnableHook(nullptr);

    return 1;
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        CreateThread(nullptr, 0, InitThread, nullptr, 0, nullptr);
        CreateThread(nullptr, 0, InsertCardThread, nullptr, 0, nullptr);
        CreateThread(nullptr, 0, CardFunctions, nullptr, 0, nullptr);
        break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
	
    return TRUE;
}

